public class SalariesEmployee extends StaffMember {

    private double salary;
    private double bonus;

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    public SalariesEmployee(int id, String name, String address, double salary, double bonus){
        super(id, name, address);
        this.salary = salary;
        this.bonus = bonus;
    }
    public String toString(){
        return  "==> Salaries Employee <==\n" +
                "ID          : " + id +
                "\nName        : " + name +
                "\nAddress     : " + address +
                "\nSalary      : " + salary + "$" +
                "\nBonus       : " + bonus + "$" +
                "\nPayment     : " + pay() + "$";
    }
    public double pay(){
        return getSalary() + getBonus();
    }
}
