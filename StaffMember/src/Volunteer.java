public class Volunteer  extends StaffMember{
    Volunteer(int id, String name, String address){
        super(id, name, address);
    }
    public String toString(){
        return  "==> Volunteer <==\n" +
                "ID          : " + id +
                "\nName        : " + name +
                "\nAddress     : " + address;
    }
    public double pay() {
        return 0;
    }
}
