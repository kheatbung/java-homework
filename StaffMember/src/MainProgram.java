import java.io.StringBufferInputStream;
import java.util.*;

public abstract class MainProgram {

    static private int id;
    static private String name;
    static private String address;
    static private double salary;
    static private double bonus;
    static private int hoursWorked;
    static private double rate;

    static Scanner sc = new Scanner(System.in).useDelimiter("\n");
    static ArrayList<StaffMember> arrayStaffMember = new ArrayList<>();
    //    StaffMember staffMember;
    //    ArrayList<Volunteer> arrayVolunteer = new ArrayList<>();
    //    ArrayList<SalariesEmployee> arraySalariesEmployee = new ArrayList<>();
    //    ArrayList<HourlyEmployee> arrayHourlyEmployee = new ArrayList<>();

    public static void main(String[] args) {
        Volunteer volunteer = new Volunteer(1,"Bun", "Siem Reap");
        HourlyEmployee hourlyEmployee = new HourlyEmployee(2, "Neth", "Siem Reap", 240, 15);
        SalariesEmployee salariesEmployee = new SalariesEmployee(3, "Jupiter", "Phnom Penh", 500, 200);
        arrayStaffMember.add(volunteer);
        arrayStaffMember.add(hourlyEmployee);
        arrayStaffMember.add(salariesEmployee);
        while (true) {
            for (int i = 0; i < arrayStaffMember.size(); i++) {
                System.out.println("\n==============================");
                System.out.println(arrayStaffMember.get(i).toString());
            }
            System.out.println("\n==============================");
            System.out.println("1. Add Employee");
            System.out.println("2. Edit");
            System.out.println("3. Remove");
            System.out.println("4. Exit");
            System.out.print("=> Please Choose Option (1-4) : " + "");
            int number = sc.nextInt();
            switch (number) {
                case 1:
                    addEmployee();
                    break;
                case 2:
                    edit();
                    break;
                case 3:
                    remove();
                    break;
                case 4:
                    System.exit(0);
                    break;
                default:
                    System.out.println("=> Option Is Invalid");
            }
        }
    }
    public static void addEmployee(){

        System.out.println("\n==============================");
        System.out.println("1. Volunteer");
        System.out.println("2. Hourly Employee");
        System.out.println("3. Salaries Employee");
        System.out.println("4. Back");
        System.out.print("=> Please Choose Option (1-4) : " + "");
        int number = sc.nextInt();
        switch (number){
            case 1: volunteer(); break;
            case 2: hourlyEmployee(); break;
            case 3: salariesEmployee(); break;
            case 4: return;
            default:
                System.out.println("=> Option Is Invalid");
        }
    }

    public static void volunteer() {
        System.out.println("\n========== INSERT INFO ==========");
        System.out.print("\n=> Enter Staff Member's ID : " + "");
        id = sc.nextInt();
        for (int i = 0; i < arrayStaffMember.size(); i++) {
            if (arrayStaffMember.get(i).getId() == id) {
                System.out.println("=>ID " + id + " Has Already Exist! Please Choose Another ID");
                break;
            } else {
                System.out.print("=> Enter Staff Member's Name : " + "");
                name = sc.next();
                System.out.print("=> Enter Staff Member's Address : " + "");
                address = sc.next();
                arrayStaffMember.add(new Volunteer(id, name, address));
                System.out.println("=> This ID (" + id + ") Add Successfully!\n");
              break;
            }
        }
    }

    public static void  hourlyEmployee(){
        System.out.println("\n========== INSERT INFO ==========");
        System.out.print("\n=> Enter Staff Member's ID : " + "");
        id = sc.nextInt();
        for (int i = 0; i < arrayStaffMember.size(); i++) {
            if (arrayStaffMember.get(i).getId() == id) {
                System.out.println("\n=>ID " + id + " Has Already Exist! Please Choose Another ID");
                break;
            } else {
                System.out.print("=> Enter Staff Member's Name : " + "");
                name = sc.next();
                System.out.print("=> Enter Staff Member's Address : " + "");
                address = sc.next();
                System.out.print("=> Enter Hours Worked : " + "");
                hoursWorked = sc.nextInt();
                System.out.print("=> Enter Rate : " + "");
                rate = sc.nextDouble();
                arrayStaffMember.add(new HourlyEmployee(id, name, address,hoursWorked,rate));
                System.out.println("=> This ID (" + id + ") Add Successfully!\n");
                break;
            }
        }
    }

    public static void salariesEmployee(){
        System.out.println("\n========== INSERT INFO ==========");
        System.out.print("\n=> Enter Staff Member's ID : " + "");
        id = sc.nextInt();
        for (int i = 0; i < arrayStaffMember.size(); i++) {
            if (arrayStaffMember.get(i).getId() == id) {
                System.out.println("\n=>ID " + id + " Has Already Exist! Please Choose Another ID");
                break;
            } else {
                System.out.print("=> Enter Staff Member's Name : " + "");
                name = sc.next();
                System.out.print("=> Enter Staff Member's Address : " + "");
                address = sc.next();
                System.out.print("=> Enter Staff's Salary : " + "");
                salary = sc.nextDouble();
                System.out.print("=> Enter Staff's Bonus : " + "");
                bonus = sc.nextDouble();
                arrayStaffMember.add(new SalariesEmployee(id, name, address,salary,bonus));
                System.out.println("=> This ID (" + id + ") Add Successfully!\n");
                break;
            }
        }
    }

    public static void edit(){
        System.out.println("\n========== EDIT INFO ==========");
        System.out.print("=> Enter Employee ID to Update : " + "");
        id = sc.nextInt();
        System.out.println("\n========== NEW INFORMATION OF STAFF MEMBER ==========");
        for(int i = 0; i < arrayStaffMember.size(); i++){
            if(arrayStaffMember.get(i).getId() == id ){
                if(arrayStaffMember.get(i).getClass().getName() == "Volunteer"){
                    System.out.print("\n=> Enter Staff's Member Name : " + "");
                    name = sc.next();
                    System.out.print("=> Enter Staff's Member Address : " + "");
                    address = sc.next();
                    arrayStaffMember.get(i).setName(name);
                    arrayStaffMember.get(i).setAddress(address);
                    System.out.println("=> ID " + id + " Update Successfully!");
                }
                if (arrayStaffMember.get(i).getClass().getName() == "HourlyEmployee"){
                    System.out.print("\n=> Enter Staff's Member Name : " + "");
                    name = sc.next();
                    System.out.print("=> Enter Staff's Member Address : " + "");
                    address = sc.next();
                    System.out.print("=> Enter Hours Worked : " + "");
                    hoursWorked = sc.nextInt();
                    System.out.print("=> Enter Rate : " + "");
                    rate = sc.nextDouble();
                    arrayStaffMember.set(i, new HourlyEmployee(id,name,address,hoursWorked,rate));
                    System.out.println("=> ID " + id + " Update Successfully!");
                }
                if(arrayStaffMember.get(i).getClass().getName() == "SalariesEmployee"){
                    System.out.print("\n=> Enter Staff's Member Name : " + "");
                    name = sc.next();
                    System.out.print("=> Enter Staff's Member Address : " + "");
                    address = sc.next();
                    System.out.print("=> Enter Salary : " + "");
                    salary = sc.nextDouble();
                    System.out.print("=> Enter Bonus : " + "");
                    bonus = sc.nextDouble();
                    arrayStaffMember.set(i, new SalariesEmployee(id,name,address,salary,bonus));
                    System.out.println("=> This ID(" + id + ") Was Updated Successfully!");
                }
            }
        }
    }

    public static void remove() {
        System.out.println("\n========== DELETE ==========");
        System.out.print("\n=> Enter Employee ID to Remove : " + "");
        id = sc.nextInt();
        for (int i = 0; i < arrayStaffMember.size(); i++) {
            if (id == arrayStaffMember.get(i).getId()) {
                if (arrayStaffMember.get(i) != null) {
                    arrayStaffMember.remove(i);
                    System.out.println("=> ID " + id + " Was Removed Successfully!");
                    return;
                }
                else {
                    System.out.println("=> This ID (" + id + ") Doesn't Exist! Please Find Another ID");
                }
            }
        }
    }

//    public static void sort(){
//        for(int i = 0; i< arrayStaffMember.size(); i++){
//            String n = arrayStaffMember.get(i).getName();
//            for(String a : n){
//
//            }
//        }
//    }
}
