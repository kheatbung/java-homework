public class HourlyEmployee extends StaffMember{
    private int hoursWorked;
    private double rate;

    public int getHoursWorked() {
        return hoursWorked;
    }

    public void setHoursWorked(int hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public HourlyEmployee(int id, String name, String address, int hoursWorked, double rate){
        super(id, name, address);
        this.hoursWorked = hoursWorked;
        this.rate = rate;
    }
    public  String toString(){
        return  "==> Hourly Employee <==\n" +
                "ID          : " + id +
                "\nName        : " + name +
                "\nAddress     : " + address +
                "\nHoursWorked : " + hoursWorked + "h" +
                "\nRate        : " + rate + "$/h" +
                "\nPayment     : " + pay() + "$";
    }
    public double pay(){
        return this.getHoursWorked() * getRate();
    }
}
